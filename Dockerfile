FROM alpine:latest

# Add edge repos. 

RUN echo "http://nl.alpinelinux.org/alpine/edge/main" >> /etc/apk/repositories && \
    echo "http://nl.alpinelinux.org/alpine/edge/community" >> /etc/apk/repositories

# Setup Vala Stuff.
RUN apk update
RUN apk --no-cache add ca-certificates vala vala-lint gcc make autoconf automake libtool gtk+3.0-dev gtk+3.0-dbg build-base git libgee-dev apk-tools bison flex autoconf-archive json-glib-dev json-glib meson ninja

